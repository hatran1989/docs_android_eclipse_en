.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Login and Register
==============================================

==========================
I.Login
==========================

1. Add permission into file Android manifest:

** Add permission connect internet ::

	  <uses-permission android:name="android.permission.INTERNET" />

	
** Add activity into file::

   <activity
            android:name="com.gtv.gtvsdkeclipse.GametvConnectActivity"
            android:exported="true"
            android:launchMode="singleTask"
            android:screenOrientation="portrait" >
        </activity>
        
          <meta-data
            android:name="com.facebook.sdk.ApplicationId"
            android:value="@string/facebook_id" />
        <activity
            android:name="com.facebook.LoginActivity"
            android:configChanges="keyboard|keyboardHidden|screenLayout|screenSize|orientation"
            android:label="@string/app_name"
            android:theme="@android:style/Theme.Translucent.NoTitleBar"
            android:screenOrientation="portrait" />
		

		
2. Open Your's Activity add block code such as:

** You create one button and setOnClickListener to call our Activity like this::

 Button btnLogin = (Button) findViewById(R.id.btn_login);
		btnLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent mIntent = new Intent(DemoActivity.this,GametvConnectActivity.class);
				mIntent.putExtra(ConstantDemo.KEY_CONNECT,ConstantDemo.KEY_LOGIN);
				startActivityForResult(mIntent, REQUEST_CODE);
			}
		});
		
**  When call our Actitivy,in onActivityResult when send for you one String "user_hash" in function Activity Result:: 

  @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == REQUEST_CODE && data != null) {
				userHash= data.getStringExtra(ConstantDemo.KEY_HASK);
				Toast.makeText(this, userHash, Toast.LENGTH_SHORT).show();

			}
		}

	}
	
You can save string user_hask to SharedPreferences for save,delete it.
  


===============================
II.Register And FastRegister
===============================

2. Open Your's Activity add block code:

** You create one button and add block code below.It's call our activity::

 Button btnLogin = (Button) findViewById(R.id.btn_login);
		btnLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent mIntent = new Intent(DemoActivity.this,GametvConnectActivity.class);
				mIntent.putExtra(ConstantDemo.KEY_CONNECT,ConstantDemo.KEY_LOGIN);
				startActivityForResult(mIntent, REQUEST_CODE);
			}
		});
   
Change Constant.KEY_LOGIN by Constant.KEY_REGISTER or Constant.KEY_REGISTER_FAST.

**  When call our Actitivy,in onActivityResult when send for you one String "user_hask" in function Activity Result:: 

  @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == REQUEST_CODE && data != null) {
				userHask = data.getStringExtra(ConstantDemo.KEY_HASK);
				Toast.makeText(this, userHask, Toast.LENGTH_SHORT).show();

			}
		}

	}
	
You can save string user_hask to SharedPreferences for save,delete it.
	



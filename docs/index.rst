.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gametv Sdk Android Documentation
==============================================
             GametvSdk_12345678
			 

.. toctree::
   :maxdepth: 2

   Overview
   LoginAndRegister
   FacebookToken
   Notification
   Payment

=========================
I.Download
=========================

**Link here:**

==========================
II.Setup Info
==========================

GametvSdk send for you all information include: gameId, package name, FacebookAppId,payment,list api,... allthing have found in documentation.If you don't understand please contact with Techsupport mycompany.
In this version you need to take care string user_hash we send for you when login, register with Webview our company.

==========================
III.Add Sdk to Project
==========================

1. Add project and libs in workspace like this: 

.. image:: images/1.import_libs.PNG

2. Click mouse right in project>> select Properties 

.. image:: images/2.import_libs.PNG

3. Add libs in workspace and click apply button.

4. Go to file Manifest.xml add::


    <uses-permission android:name="android.permission.INTERNET" />
  
     <activity
            android:name="com.gtv.gtvsdkeclipse.GametvConnectActivity"
            android:exported="true"
            android:launchMode="singleTask"
            android:screenOrientation="portrait" >
        </activity>
        
          <meta-data
            android:name="com.facebook.sdk.ApplicationId"
            android:value="@string/facebook_id" />
        <activity
            android:name="com.facebook.LoginActivity"
            android:configChanges="keyboard|keyboardHidden|screenLayout|screenSize|orientation"
            android:label="@string/app_name"
            android:theme="@android:style/Theme.Translucent.NoTitleBar"
            android:screenOrientation="portrait" />
  

  
5. Go to YourActivity we setup for get String user_hask like this::

 
     public class DemoActivity extends Activity {

	private static final int REQUEST_CODE = 101;
	private static String TAG = DemoActivity.class.getSimpleName();
	private LoginButton loginBtn;
	private UiLifecycleHelper uiHelper;
	private static String userHash = ConstantDemo.EMPTY;

	private Session.StatusCallback statusCallback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, final SessionState state,
				final Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		uiHelper = new UiLifecycleHelper(this, statusCallback);
		uiHelper.onCreate(savedInstanceState);
		Session session = Session.getActiveSession();
		session.closeAndClearTokenInformation();

		Button btnLogin = (Button) findViewById(R.id.btn_login);
		btnLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent mIntent = new Intent(DemoActivity.this,GametvConnectActivity.class);
				mIntent.putExtra(ConstantDemo.KEY_CONNECT,ConstantDemo.KEY_LOGIN);
				startActivityForResult(mIntent, REQUEST_CODE);
			}
		});

		loginBtn = (LoginButton) findViewById(R.id.fb_login_button);
		loginBtn.setReadPermissions(Arrays.asList("public_profile",
				"user_likes", "user_friends", "user_status", "email",
				"user_birthday"));		
		loginBtn.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
		
		loginBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
			}
		});

		Intent intent = getIntent();
		if (intent != null) {
			String checkLogout = intent.getStringExtra("check");
			if (!TextUtils.isEmpty(checkLogout)) {


			}

		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == REQUEST_CODE && data != null) {
				userHash = data.getStringExtra(ConstantDemo.KEY_HASK);
				Toast.makeText(this, userHash, Toast.LENGTH_SHORT).show();

			}
		}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}

		uiHelper.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	private void onSessionStateChange(final Session session,
			SessionState state, Exception exception) {
		if (state.isOpened()) {

			Request request = Request.newMeRequest(session,
					new Request.GraphUserCallback() {

						@Override
						public void onCompleted(GraphUser user,
								Response response) {
							if (session == Session.getActiveSession()) {

								if (user != null) {
									//Log.e(TAG + "FB access token: ",session.getAccessToken());									
									if(userHask.isEmpty()){
										Intent mIntent = new Intent(DemoActivity.this,GametvConnectActivity.class);
										mIntent.putExtra(ConstantDemo.KEY_CONNECT,ConstantDemo.KEY_LOGIN_FACEBOOK);
										mIntent.putExtra(ConstantDemo.KEY_FACEBOOK_HASK,session.getAccessToken());
										startActivityForResult(mIntent, REQUEST_CODE);
									}									

								} else {

								}
							}
						}

					});
			Bundle params = new Bundle();
			params.putString("fields",
					"id, first_name, last_name, picture, email, name, birthday");
			request.setParameters(params);
			request.executeAsync();
		} else if (state.isClosed()) {
			Log.e(TAG, "Logout");
		}
	}

	/** Logout Facebook */
	public static void callFacebookLogout(Context context) {

		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear preferences if save
				userHash = ConstantDemo.EMPTY;
			}
		} else {
			session = new Session(context);
			Session.setActiveSession(session);
			session.closeAndClearTokenInformation();
			// clear prerence if saved
		}
	}

  }
  
 

We send for you String "user_hask" in funtion "onActivityResult" ::
 
   userHask = data.getStringExtra(ConstantDemo.KEY_HASK);
   
6. Don't forget create button in file xml::

    <Button
        android:id="@+id/btn_login"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginBottom="20dp"
        android:layout_marginTop="20dp"
        android:text="Login" />

    <com.facebook.widget.LoginButton
        android:id="@+id/fb_login_button"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
       />
	   

7. Add line code into file "strings.xml" ::

       <string name="facebook_id">2292283651024631</string>
	
You have finish add library to your project.





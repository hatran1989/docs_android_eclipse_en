.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FacebookToken 
==============================================

1. Open Your's Activity add block code such as:

** Go to file xml add button facebook::

 <com.facebook.widget.LoginButton
        android:id="@+id/fb_login_button"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
       />
	   
** You create one button in Activity like this::

        private LoginButton loginBtn;
        private UiLifecycleHelper uiHelper;
		
		private Session.StatusCallback statusCallback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, final SessionState state,
				final Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};
 
        uiHelper = new UiLifecycleHelper(this, statusCallback);
		uiHelper.onCreate(savedInstanceState);
		Session session = Session.getActiveSession();
		session.closeAndClearTokenInformation();
		
		loginBtn = (LoginButton) findViewById(R.id.fb_login_button);
		loginBtn.setReadPermissions(Arrays.asList("public_profile",
				"user_likes", "user_friends", "user_status", "email",
				"user_birthday"));		
		loginBtn.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
		
**  When call our Actitivy,in onActivityResult when send for you one String "user_hash" in function Activity Result:: 

  @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == REQUEST_CODE && data != null) {
				userHask = data.getStringExtra(ConstantDemo.KEY_HASK);
				Toast.makeText(this, userHask, Toast.LENGTH_SHORT).show();

			}
		}

	}

	
	
You can save string user_hask to SharedPreferences for save,delete it.